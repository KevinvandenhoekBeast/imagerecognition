//
//  ViewController.swift
//  KopieID
//
//  Created by Kevin van den Hoek on 25/11/2018.
//  Copyright © 2018 Triple IT. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        initialize()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func initialize() {
        // Override
    }
    
    func setup() {
        view.backgroundColor = UIColor.black
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return UIStatusBarStyle.lightContent }
}
