//
//  VisionTextBoxViewController.swift
//  KopieID
//
//  Created by Kevin van den Hoek on 25/11/2018.
//  Copyright © 2018 Triple IT. All rights reserved.
//

import UIKit
import Vision
import ARKit

class VisionTextBoxViewController: ARViewController {
    
    private let visionQueue = DispatchQueue(label: (Bundle.main.bundleIdentifier ?? "") + ".serialVisionQueue")
    private var characterBoxes: [UIView] = []
    
    override func sessionDidUpdate(_ session: ARSession, frame: ARFrame) {
        classifyCurrentImage()
    }
}

private extension VisionTextBoxViewController {
    
    func classifyCurrentImage() {
        //guard let orientation = CGImagePropertyOrientation(rawValue: UInt32(UIDevice.current.orientation.rawValue)) else { return }
        guard let orientation = CGImagePropertyOrientation(rawValue: UInt32(6)) else { return }
        guard let currentBuffer = currentBuffer else { return }
        //let coreImage = UIImage(ciImage: CIImage(cvImageBuffer: currentBuffer))
        let requestHandler = VNImageRequestHandler(cvPixelBuffer: currentBuffer, orientation: orientation)
        visionQueue.async {
            do {
                defer { self.currentBuffer = nil } // Release the pixel buffer when done, allowing the next buffer to be processed.
                let request = VNDetectTextRectanglesRequest(completionHandler: { [weak self] (request, error) in
                    if let error = error { print("Request error: \(error)") }
                    self?.processImageRequestResults(request.results)
                })
                request.reportCharacterBoxes = true
                try requestHandler.perform([request])
            } catch { print("Error: Vision request failed with error \"\(error)\"") }
        }
    }
    
    func processImageRequestResults(_ results: [Any]?) {
        guard let results = results as? [VNTextObservation] else { return }
        var characterBoxes: [VNRectangleObservation] = []
        var words: [[VNRectangleObservation]] = []
        for result in results {
            guard let newCharacterBoxes = result.characterBoxes else { continue }
            characterBoxes += newCharacterBoxes
            words.append(newCharacterBoxes)
        }
        DispatchQueue.main.async { [weak self] in
            self?.renderCharacterBoxes(characterBoxes)
            self?.captureWords(words)
        }
    }
    
    func renderCharacterBoxes(_ boxes: [VNRectangleObservation]) {
        guard let currentImageSize = self.currentImageSize else { return }
        print("current image size: \(currentImageSize)")
        self.characterBoxes.forEach( { $0.removeFromSuperview() })
        self.characterBoxes = []
        for box in boxes {
            let boxView = UIView(
                frame: getFrame(for: box)
            )
            boxView.layer.borderWidth = 1
            boxView.layer.borderColor = UIColor.white.withAlphaComponent(0.7).cgColor
            boxView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            view.addSubview(boxView)
            self.characterBoxes.append(boxView)
        }
    }
    
    func captureWords(_ words: [[VNRectangleObservation]]) {
        for word in words {
            for character in word {
                
            }
        }
    }
    
    func getFrame(for rectangle: VNRectangleObservation) -> CGRect {
        guard let currentImageSize = self.currentImageSize else { return CGRect.zero }
        return CGRect(
            x: (rectangle.bottomLeft.x * currentImageSize.width) + currentImageOffsetX,
            y: ((1 - rectangle.bottomLeft.y) * currentImageSize.height) + currentImageOffsetY,
            width: (rectangle.bottomRight.x - rectangle.bottomLeft.x) * currentImageSize.width,
            height: (rectangle.bottomLeft.y - rectangle.topLeft.y) * currentImageSize.height
        )
    }
}
