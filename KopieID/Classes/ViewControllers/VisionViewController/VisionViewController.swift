//
//  VisionViewController.swift
//  KopieID
//
//  Created by Kevin van den Hoek on 25/11/2018.
//  Copyright © 2018 Triple IT. All rights reserved.
//

import UIKit
import Vision
import ARKit

class VisionViewController: ARViewController {
    
    private let visionQueue = DispatchQueue(label: (Bundle.main.bundleIdentifier ?? "") + ".serialVisionQueue")
    private(set) var rectangles: [CAShapeLayer] = []
    
    override func sessionDidUpdate(_ session: ARSession, frame: ARFrame) {
        classifyCurrentImage()
    }
    
    func didUpdateRectangles() {
        // Override
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cardOverlayView.isHidden = true
    }
}

private extension VisionViewController {
    
    func classifyCurrentImage() {
        //guard let orientation = CGImagePropertyOrientation(rawValue: UInt32(UIDevice.current.orientation.rawValue)) else { return }
        guard let orientation = CGImagePropertyOrientation(rawValue: UInt32(6)) else { return }
        guard let currentBuffer = currentBuffer else { return }
        //let coreImage = UIImage(ciImage: CIImage(cvImageBuffer: currentBuffer))
        let requestHandler = VNImageRequestHandler(cvPixelBuffer: currentBuffer, orientation: orientation)
        visionQueue.async {
            do {
                defer { self.currentBuffer = nil } // Release the pixel buffer when done, allowing the next buffer to be processed.
                let request = VNDetectRectanglesRequest(completionHandler: { [weak self] (request, error) in
                    if let error = error { print("Request error: \(error)") }
                    self?.processImageRequestResults(request.results)
                })
                try requestHandler.perform([request])
            } catch { print("Error: Vision request failed with error \"\(error)\"") }
        }
    }
    
    func processImageRequestResults(_ results: [Any]?) {
        guard let results = results as? [VNRectangleObservation] else { return }
        DispatchQueue.main.async { [weak self] in
            self?.renderCharacterBoxes(results)
        }
    }
    
    func renderCharacterBoxes(_ boxes: [VNRectangleObservation]) {
        self.rectangles.forEach( { $0.removeFromSuperlayer() })
        self.rectangles = []
        for box in boxes {
            let boxView = CAShapeLayer()
            boxView.fillColor = UIColor.green.cgColor
            boxView.path = getPath(for: box)
            //boxView.frame = CGRect(x: 0, y: 0, width: compensatedWidth, height: view.bounds.height)
            view.layer.addSublayer(boxView)
            self.rectangles.append(boxView)
        }
        didUpdateRectangles()
    }
    
    func getPath(for rectangle: VNRectangleObservation) -> CGPath {
        let path = UIBezierPath()
        path.move(to: getConvertedPoint(rectangle.topLeft))
        path.addLine(to: getConvertedPoint(rectangle.topRight))
        path.addLine(to: getConvertedPoint(rectangle.bottomRight))
        path.addLine(to: getConvertedPoint(rectangle.bottomLeft))
        path.close()
        return path.cgPath
    }
    
    func getConvertedPoint(_ fraction: CGPoint) -> CGPoint {
        let compensatedWidth = view.bounds.width * 1.5
        let widthOvershoot = compensatedWidth - view.bounds.width
        return CGPoint(x: fraction.x * compensatedWidth - (widthOvershoot * 0.5), y: view.bounds.height - (fraction.y * view.bounds.height))
    }
    
    func getFrame(for rectangle: VNRectangleObservation) -> CGRect {
        guard let currentImageSize = self.currentImageSize else { return CGRect.zero }
        print("topLeft: \(rectangle.topLeft), \(rectangle.topRight)")
        return CGRect(
            x: (rectangle.bottomLeft.x * currentImageSize.width) + currentImageOffsetX,
            y: ((1 - rectangle.bottomLeft.y) * currentImageSize.height) + currentImageOffsetY,
            width: (rectangle.bottomRight.x - rectangle.bottomLeft.x) * currentImageSize.width,
            height: (rectangle.bottomLeft.y - rectangle.topLeft.y) * currentImageSize.height
        )
    }
}
