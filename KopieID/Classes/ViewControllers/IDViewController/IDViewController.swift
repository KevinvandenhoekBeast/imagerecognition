//
//  IDViewController.swift
//  KopieID
//
//  Created by Kevin van den Hoek on 25/11/2018.
//  Copyright © 2018 Triple IT. All rights reserved.
//

import FirebaseMLVision
import ARKit

class IDViewController: VisionViewController {
    
    private var isProcessing: Bool = false
    
    private var textFrames: [UIView] = []
    
    override func sessionDidUpdate(_ session: ARSession, frame: ARFrame) {
        super.sessionDidUpdate(session, frame: frame)
        update()
    }
    
    func update() {
        guard let sampleBuffer = self.getSampleBuffer(from: self.currentBuffer) else { return }
        //let image: UIImage = UIImage(ciImage: CIImage(cvImageBuffer: buffer))
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            return
            //self.analyzeImageIfIdle(sampleBuffer)
        }
    }
    
    override func didUpdateRectangles() {
        super.didUpdateRectangles()
        updateRectangleStatus()
    }
    
    func analyzeImageIfIdle(_ buffer: CMSampleBuffer) {
        guard !isProcessing else {
            print("Already processing")
            return
        }
        print("Starting processing")
        self.isProcessing = true
        let vision = Vision.vision()
        let textRecognizer = vision.onDeviceTextRecognizer()
        let visionImage = VisionImage(buffer: buffer)
        let metadata = VisionImageMetadata()
        metadata.orientation = VisionDetectorImageOrientation.rightTop
        visionImage.metadata = metadata
        textRecognizer.process(visionImage) { (text, error) in
            print("Finished processing")
            self.isProcessing = false
            if let text = text {
                print("Successfully read image")
                self.processVisionText(text)
            } else if let error = error {
                print("Error: \(error.localizedDescription)")
            } else {
                print("Unknown error")
            }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.update()
            }
        }
    }
    
    func processVisionText(_ visionText: VisionText) {
        textFrames.forEach({ $0.removeFromSuperview() })
        textFrames = []
        
        visionText.blocks.forEach { (textBlock) in
            print("visionText: \(textBlock.text)")
            let textFrame = UIView()
            textFrame.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            textFrame.layer.borderWidth = 1
            textFrame.layer.borderColor = UIColor.white.cgColor
            let convertedFrame = CGRect(x: textBlock.frame.minY, y: textBlock.frame.minX, width: textBlock.frame.height, height: textBlock.frame.width)
            
            textFrame.frame = getFrame(for: convertedFrame)
            if textBlock.text.count == 9 {
                textFrame.backgroundColor = UIColor.black
                textFrame.layer.borderColor = UIColor.clear.cgColor
            }
            textFrames.append(textFrame)
            view.addSubview(textFrame)
        }
    }
}

private extension IDViewController {
    
    func getFrame(for rectangle: CGRect) -> CGRect {
        guard let currentImageSize = self.currentImageSize else { return CGRect.zero }
        return CGRect(
            x: (rectangle.minX * currentImageSize.width) + currentImageOffsetX,
            y: (rectangle.minY * currentImageSize.height) + currentImageOffsetY,
            width: rectangle.width * currentImageSize.width,
            height: rectangle.height * currentImageSize.height
        )
    }
    
    func getSampleBuffer(from pixelBuffer: CVPixelBuffer?) -> CMSampleBuffer? {
        guard let pixelBuffer = pixelBuffer else { return nil }
        var newSampleBuffer: CMSampleBuffer? = nil
        var timimgInfo: CMSampleTimingInfo = CMSampleTimingInfo.invalid
        var videoInfo: CMVideoFormatDescription? = nil
        CMVideoFormatDescriptionCreateForImageBuffer(allocator: nil, imageBuffer: pixelBuffer, formatDescriptionOut: &videoInfo)
        CMSampleBufferCreateForImageBuffer(allocator: kCFAllocatorDefault, imageBuffer: pixelBuffer, dataReady: true, makeDataReadyCallback: nil, refcon: nil, formatDescription: videoInfo!, sampleTiming: &timimgInfo, sampleBufferOut: &newSampleBuffer)
        return newSampleBuffer
    }
    
    func captureImage(view: UIView) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        let snapshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return snapshotImage
    }
}

private extension IDViewController {
    func updateRectangleStatus() {
        guard self.rectangles.count > 0 else {
            blurView.backgroundColor = UIColor.green.withAlphaComponent(0)
            return
        }
        var previousOffset: CGFloat = 0
        for rectangle in self.rectangles {
            let yOffset = abs(rectangle.frame.midY - (view.frame.height / 2))
            let widthOffset = abs(rectangle.frame.width - cardOverlayView.frame.width)
            let marginForError: CGFloat = 30
            let deadZone: CGFloat = 20
            let error = (yOffset + widthOffset) - deadZone
            let offset = (marginForError - error) / marginForError
            previousOffset = min(max(previousOffset, offset), 1)
            blurView.backgroundColor = UIColor.green.withAlphaComponent(previousOffset * 1)
        }
    }
}
