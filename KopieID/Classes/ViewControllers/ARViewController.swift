//
//  ViewController.swift
//  KopieID
//
//  Created by Kevin van den Hoek on 25/11/2018.
//  Copyright © 2018 Triple IT. All rights reserved.
//

import UIKit
import ARKit
import SpriteKit
import Vision
import EasyPeasy
import CoreImage

class ARViewController: ViewController {
    
    let sceneView: ARSKView = ARSKView(frame: CGRect.zero)
    let overlayScene = SKScene()
    
    var currentBuffer: CVPixelBuffer? // The pixel buffer being held for analysis; used to serialize Vision requests.
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return UIStatusBarStyle.lightContent }
    
    private(set) var currentImageSize: CGSize?
    private(set) var currentImageOffsetX: CGFloat = 0
    private(set) var currentImageOffsetY: CGFloat = 0
    
    let cardOverlayView = UIImageView(image: UIImage(named: "card_overlay"))
    let blurView = UIVisualEffectView(effect: UIBlurEffect(style: .light))
    private let blurMaskView = UIView()
    
    override func setup() {
        super.setup()
        
        view.addSubview(sceneView)
        sceneView.easy.layout([Edges()])
        sceneView.backgroundColor = UIColor.clear
        
        overlayScene.scaleMode = .aspectFill
        sceneView.delegate = self
        sceneView.presentScene(overlayScene)
        sceneView.session.delegate = self
        
        view.addSubview(blurView)
        blurView.easy.layout([Edges()])
        
        view.addSubview(blurMaskView)
        blurMaskView.easy.layout([Edges()])
        blurMaskView.backgroundColor = .clear
        blurView.mask = blurMaskView
        
        blurMaskView.addSubview(cardOverlayView)
        cardOverlayView.easy.layout([Width(-50).like(self.view, .width), Height(-50).like(self.view, .height), CenterY(), CenterX()])
        cardOverlayView.contentMode = .scaleAspectFit
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let configuration = ARWorldTrackingConfiguration()
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        sceneView.session.pause()
    }
    
    func sessionDidUpdate(_ session: ARSession, frame: ARFrame) {
        // TODO: Override
    }
}

extension ARViewController: ARSKViewDelegate {
    func session(_ session: ARSession, didUpdate frame: ARFrame) {
        guard currentBuffer == nil, case .normal = frame.camera.trackingState else {
            return
        }
        
        // Retain the image buffer for Vision processing.
        self.currentBuffer = frame.capturedImage
        if self.currentImageSize == nil {
            let resolution = CGSize(width: frame.camera.imageResolution.height, height: frame.camera.imageResolution.width)
            let imageAspect = resolution.width / resolution.height
            let renderAspect = view.frame.width / view.frame.height
            if imageAspect > renderAspect { // Captured image is relatively wider
                let sizeRatio = view.frame.height / resolution.height
                let width = resolution.width * sizeRatio
                self.currentImageSize = CGSize(width: width, height: resolution.height * sizeRatio)
                self.currentImageOffsetX = -(width - view.frame.width) / 2
            } else { // Captured image is relatively higher
                let sizeRatio = view.frame.width / resolution.width
                let height = resolution.height * sizeRatio
                self.currentImageSize = CGSize(width: resolution.width * sizeRatio, height: height)
                self.currentImageOffsetY = -(height - view.frame.height) / 2
            }
        }
        sessionDidUpdate(session, frame: frame)
    }
}

extension ARViewController: ARSessionDelegate {
    
}
